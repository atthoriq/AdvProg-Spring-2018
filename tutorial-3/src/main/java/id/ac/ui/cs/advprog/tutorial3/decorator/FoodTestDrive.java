package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;

public class FoodTestDrive {
    public static void main(String[] args) {
        Food thoriqSandwich = new CrustySandwich();

        System.out.println(thoriqSandwich.getDescription()+ " " + thoriqSandwich.cost());

        thoriqSandwich = new BeefMeat(thoriqSandwich);
        thoriqSandwich = new Cheese(thoriqSandwich);
        System.out.println(thoriqSandwich.cost());
    }
}
