package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class CompanyTestDrive {
    public static void main(String[] args) {
        Employees uiuxDesigner = new UiUxDesigner("Thoriq", 1000000);
        Employees securityExp = new SecurityExpert("Adib", 150000);
        Employees networkExp = new NetworkExpert("Zain", 170000);
        Employees frontend = new FrontendProgrammer("Bagas", 120000);
        Employees backend = new BackendProgrammer("Wigo", 190000);

        Company fasilkom = new Company();

        fasilkom.addEmployee(uiuxDesigner);
        fasilkom.addEmployee(securityExp);
        fasilkom.addEmployee(networkExp);
        fasilkom.addEmployee(frontend);
        fasilkom.addEmployee(backend);

        System.out.println(fasilkom.getNetSalaries());

        for (Employees e: fasilkom.getAllEmployees()) {
            System.out.println(e.name);
        }
    }
}
