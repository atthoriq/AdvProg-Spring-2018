package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    // TODO Complete me!
    public ModelDuck() {
        this.setFlyBehavior(new FlyNoWay());
        this.setQuackBehavior(new MuteQuack());
        this.setSwimBehavior(new Swim());
    }

    @Override
    public void display() {
        System.out.println("I am a Model Duck! :)");
    }
}
